<?php

namespace BedTech\Core\CurrencyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BedTechCoreCurrencyBundle:Default:index.html.twig', array('name' => $name));
    }
}
